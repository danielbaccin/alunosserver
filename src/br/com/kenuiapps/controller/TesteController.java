package br.com.kenuiapps.controller;

import java.util.ArrayList;
import java.util.List;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.view.Results;
import br.com.kenuiapps.model.Aluno;
import br.com.kenuiapps.model.Curso;

@Resource
@Path("/teste")
public class TesteController {
	
	private final Result result;

	public TesteController(Result result) {
		this.result = result;
	}
	
	
	@Path("/alunos")
	public void alunos() {
		List<Aluno> alunos = dadoUmaListaDeAlunos();
		result.use(Results.json()).withoutRoot().from(alunos).serialize();
	}
	
	
	private List<Aluno> dadoUmaListaDeAlunos() {
		List<Aluno> alunos = new ArrayList<Aluno>();
		Curso cursoJavaOO = new Curso(1, "Java Orientação Objetos", 70);
		Curso cursoAndroid = new Curso(2, "Android", 40);
		Curso cursoJavaWeb = new Curso(3, "Java Web", 80);
		
		Aluno alunoUm =  new Aluno(1, "Jose", 20);
		alunoUm.adicionaCurso(cursoJavaOO);
		alunoUm.adicionaCurso(cursoAndroid);
		alunoUm.adicionaCurso(cursoJavaWeb);
		alunos.add(alunoUm);

		Aluno alunoDois =  new Aluno(2, "Pedro", 20);
		alunoDois.adicionaCurso(cursoJavaOO);
		alunos.add(alunoDois);
		
		
		return alunos;
	}

}
