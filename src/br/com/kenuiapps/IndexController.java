/***
 * Copyright (c) 2009 Caelum - www.caelum.com.br/opensource
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.com.kenuiapps;

import java.util.ArrayList;
import java.util.List;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.view.Results;
import br.com.kenuiapps.model.Aluno;
import br.com.kenuiapps.model.Curso;

@Resource
public class IndexController {

	private final Result result;

	public IndexController(Result result) {
		this.result = result;
	}

	@Path("/")
	public void index() {
		result.include("variable", "VRaptor!");
	}
	
	
	@Get("/testeJson")
	public void testeJson(String paramentro1, String paramentro2) {
		List<Aluno> alunos = dadoUmaListaDeAlunos();
		result.use(Results.json()).withoutRoot().from(alunos).serialize();
	}

	private List<Aluno> dadoUmaListaDeAlunos() {
		List<Aluno> alunos = new ArrayList<Aluno>();
		Curso cursoJavaOO = new Curso(1, "Java Orientação Objetos", 70);
		Curso cursoAndroid = new Curso(2, "Android", 40);
		Curso cursoJavaWeb = new Curso(3, "Java Web", 80);
		
		Aluno alunoUm =  new Aluno(1, "Jose", 20);
		alunoUm.adicionaCurso(cursoJavaOO);
		alunoUm.adicionaCurso(cursoAndroid);
		alunoUm.adicionaCurso(cursoJavaWeb);

		Aluno alunoDois =  new Aluno(2, "Pedro", 20);
		alunoUm.adicionaCurso(cursoJavaOO);
		
		return alunos;
	}

}
