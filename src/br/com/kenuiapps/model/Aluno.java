package br.com.kenuiapps.model;

import java.util.ArrayList;
import java.util.List;

public class Aluno {
	
	private Long codigo;
	private String nome;
	private Integer idade;
	private List<Curso> cursos;
	
	public Aluno() {}
	
	public Aluno(long codigo, String nome, int idade) {
		this.codigo = codigo;
		this.nome = nome;
		this.idade = idade;
		cursos =  new ArrayList<Curso>();
	}
	
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Integer getIdade() {
		return idade;
	}
	public void setIdade(Integer idade) {
		this.idade = idade;
	}
	public List<Curso> getCursos() {
		return cursos;
	}
	public void setCursos(List<Curso> cursos) {
		this.cursos = cursos;
	}

	public void adicionaCurso(Curso curso) {
		cursos.add(curso);
	}
	
}
